package com.AssignmentQ2f1;

import java.util.LinkedList;
import java.util.List;

public class LinkedLists {

	public static void main(String[] args) {
		List<String> l = new LinkedList<String>();
		l.add("7Up");
		l.add("Coke");
		l.add("Limca");
		l.add("Slice");
		l.add("sprite");
		System.out.println("size: " + l.size());
		System.out.println("list contains : " + l);

		l.remove(1);
		System.out.println("after removed :" + l);

		if (l.equals("Coke")) {
			System.out.println(" The coke is present");
		} else {
			System.out.println("The coke is not  present");
		}
	}

}
