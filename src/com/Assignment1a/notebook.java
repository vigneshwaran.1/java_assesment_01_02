package com.Assignment1a;

public class notebook extends Book {

	public void draw() {

		System.out.println("Draw the picture.");

	}

	@Override
	void write() {
		System.out.println("To write the paragraph.");

	}

	@Override
	void read() {
		
		System.out.println("To read the book.");

	}
	
	public static void main(String[] args) {
		
		notebook n = new notebook();
		n.draw();
		n.write();
		n.read();
	}

}
