package com.Assignment1a;

public abstract class Book {

	abstract void write();

	abstract void read();

}
