package com.AssignmentQ2A;

public class SmartTelephone  extends Telephone{

	@Override
	public void with() {
		System.out.println("With");
		
	}

	@Override
	public void lift() {
		System.out.println("Lift");
		
	}

	@Override
	public void disconnected() {
		System.out.println("Disconnected");
		
	}
	public static void main(String[] args) {
		SmartTelephone s = new SmartTelephone();
		s.with();
		s.lift();
		s.disconnected();
	}

}
